package uz.dilshodjon216.weatherapp.api

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import uz.dilshodjon216.weatherapp.models.Weather

interface ApiService {
    @GET("weather/{city}")
    suspend fun getWeather(@Path("city") city:String):Response<Weather>

}