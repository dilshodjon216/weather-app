package uz.dilshodjon216.weatherapp.models

data class Forecast(
    val day: String,
    val temperature: String,
    val wind: String
)