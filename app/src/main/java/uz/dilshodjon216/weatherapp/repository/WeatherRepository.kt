package uz.dilshodjon216.weatherapp.repository

import uz.dilshodjon216.weatherapp.api.ApiService
import javax.inject.Inject

class WeatherRepository
@Inject
constructor(private val apiService: ApiService) {

    suspend fun getWeather(city:String) = apiService.getWeather(city)

}